+++
title = "Language Types"
date = 2016-01-07
draft = false

tags = [ "dev" ]
+++

Typing systems are a common source of confusion in novice programmers. Let's talk about them!

First of all, it is worth noting that I will not be mentioning specialized type systems (such as dependent types, union types, etc.), and will rather focus on languages, and their implementation of types.

Type systems come in multiple shapes and sizes. The primary distinctions are between the type itself, which is divided into static, dynamic and duck; strong and weak; as well as the implementation. The first two distinctions are what we will be talking about today.

## Typing Systems ##

### Static Typing ###
Static typing is when every variable has a specific type, determined at compile time (or write-time!). An example of this is the C family of languages. Consider the following (C++, since strong typing helps in this example):


```c++
int a = 5;
char b = 'b';
double c = 5.00;
```

Changing things here, such as removing the period and zeroes from the double definition would result in a compiler error. We declare what type a variable is at the time of its creation, and it never changes. While you can cast (more on this in a second), you cannot cast a variable unto itself. Consider the following:

```c++
char b = 'x';
b = (int)(b); // this will produce an error! after all, our lvalue is not of type int, while our rvalue is!
int a = (int)(b); // this is perfectly fine though
```

However, it's important to note that pure statically typed languages barely exist. Let's take the case of C++, for example, which supports casting. Downcasting (see example) actually requires some form of dynamic checking of types, effectively becoming a mixed type system. Here's a demonstration:

```c++
class A {};
class B : public A {};
int main()
{
	A* a = new B();
	B* b = a; // this doesn't work, why would it? a is of type A*! not B*
	B* b = (B*)a; //this does work, but it's a naive implementation
}
```

You realize the naivete of this approach when you twist it a bit:

```c++
class A {};
class B {};
int main()
{
	A* a = new A();
	B* b = (B*)a; // this works just fine!
}
```

Being a pure static language, C++ approaches casting from a very interesting perspective: "I don't know, but the programmer probably knows, so I'll let it happen". This is also, in part, because it is easy to convert between different types of pointers. Let's look at the exact same examples in Java.

```java
class A {}
class B extends A {}
public class Main {
	public static void main(String[] args) {
		A a = new B();
		B b = (B)a; // works fine
	}
}
```

In contrast with:

```java
class A {}
class B {}
public class Main {
	public static void main(String[] args) {
		A a = new B(); // error: incompatible types: B cannot be converted to A
		B b = (B)a; // error: incompatible types: A cannot be converted to B
	}
}
```

Static typing is useful in compile-time optimization: already knowing the type of something means you don't need expensive runtime tests to figure out what type it is, but that carries its own complexity in the end.

### Dynamic Typing ###
In dynamic typing, a variable will typically have a "tag" associated with it during runtime that contains the information on what type it is right now. It also tends to allow things that are often not at all allowed in static typing. Worthy of note is that most dynamically typed languages implement duck typing, as the two are not incompatible, but for now, let's look at this lengthy example from python.

```python
a = 5
print(a)
#=> 5
a = "twenty"
print(a)
#=> twenty

class A:
	def a(self):
		print("in A")
	pass

class B:
	def a(self):
		print("in B")
	pass

a = A()
b = B()

a.a()
#=> in A
b.a()
#=> in B

b = a
b.a()
#=> in A
```

Here we see that a and b obviously change, but they are very flexible in doing so, because they are dynamically typed.

The advantage of dynamic typing is the massive flexibility it has. However, it comes at a cost of efficiency, and in some cases, it can cause runtime failures (some languages allow recovery from it, however). Here's an example of a type problem in python:

```python
a = "abcd"
a[2] = 'x' # TypeError: 'str' object does not support item assignment
a = a[:2] + 'x' + a[3:] # works fine
b = a + 5 + "stuff" # TypeError: Can't convert 'int' object to str implicitly
b = a + str(5) + "stuff" # works fine
```

These happen because values still have types, they can simply change at will, as well as because python is strongly typed. In short, dynamic typing is when any type errors are simply reported at runtime, and types are fairly flexible in what one can do with them.

### Duck Typing ###
Duck typing comes out of an old saying: "if it walks like a duck, swims like a duck, and quacks like a duck, then it probably is a duck". This typing mechanism relies on some specific identifiable attributes of a value in order to use it as if it was another. An example implementation as proposed by me was to use reserved function names. For example, consider the following:

	class A { x="hello" }
	class B { y="byebye" }
	def A.&A(:self) { return :self }
	def B.&B(:self) { return :self }
	def B.&A(:self)
	{
		a = A
		a.x = :self.y
		return a
	}
	def A.@==(:self, :other)
	{
		a = :self.&A # if this was some class C, this would throw an error!
		b = :other.&A
		return a.x == b.x
	}
	a = A
	b = B
	c = (A)b # compiles to b.&A() which compiles to B::&A(b)
	a == c # compiles to a.@==(c) which compiles to A::@==(:self(a), :other(c))
	# => false

Many modern languages that are dynamically typed (such as python and ruby, for example) also implement duck typing in one way or another. See the python example from the previous section, notice the str(x) and the way we can print an integer.

Duck typing needs quite a bit of planning to implement properly, but can avoid some of the issues of pure dynamic typing.

### Gradual Typing ###
As of python 3.5, it is gradually typed. In gradual typing, you can define some variables as having some specific type during compile-time, but other variables may be left untyped. Thus the typing resolution happens gradually from compile to resolve times. While it is only being given brief mention here, because it is noteworthy, it should be fairly trivial to figure out given the examples above.

## Strong vs Weak ##
Strong vs Weak typing is fairly commonly mentioned by programmers, myself included. What may surprise some is that good definitions of what each is do not actually exist. The definition I use, which allows me to be understood fairly well most of the time is as follows: a strongly typed language does little implicit conversions and is not afraid to throw typing errors at any time, an example being python. A weakly typed language can do many conversions implicitly and tends not to throw type-related exceptions (possibly because it just crashes, or doesn't compile), an example being C, where many types can convert between each other (see example) implicitly and all pointers are castable to other pointers.

```c
char a = 5;
```


## Conclusion ##
In conclusion, type systems are integral to how a language functions, and choosing a proper type system comes with various tradeoffs.

