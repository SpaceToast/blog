+++
title = "Wireguard as cross-DC private WAN"
date = 2018-08-03
draft = true

tags = [ "sys", "alpine" ]
+++

It's a commonly desired thing to have a "Private WAN" between datacenters. Whether it be to enable cross-dc [consul][consul]/[nomad][nomad] deployments, NATted secure database access, or reasons mortal fools like us simply cannot understand.

Usually, this is achieved using something lacking (e.g l2tp), complex to set up and maintain (ipsec), or ridiculous (literally laying a direct fiber optic line from A to B).

[Wireguard][wireguard] is a VPN system comparable to ipsec and openvpn which features in-kernel interfaces, modern cryptography, simple setup (similar to ssh, more on this later) and performance that leaves both of the abovementioned in the dust.

However, it is also a relatively new project, with most of the documentation (at the time of writing) focusing on point-to-point client/server connections. There have been a couple of (highly questionable) "guides" online that created an increasingly large amount of interfaces for every new DC.

Here I will attempt to detail how to get the above-described setup, explain why/how it works, and propose a helper tool for this use case (that I may later go on to write).

## Background

Wireguard is similar to OpenSSH - each host has a private key and a public key, and has a list of peers (identified by their public keys) that it is familiar with.
For most intents and purposes, however, wireguard is connectionless - it will simply fire the packets at the last known endpoint.

Further, wireguard has something it calls a "Cryptokey Routing Table".
Ths means that each public key has a direct association to a list of allowed IPs.

The effects of this are not immediately obvious, so let us consider an example:

Imagine you have two LANs you want to connect - `10.1.0.0/16` and `10.2.0.0/16`.
You decide your wireguard subnet will be `172.17.0.0/24` and interface to be `wg0`.
You set up the two peers (respectively as `172.17.0.1/32` and `172.17.0.2/32` as their allowed IPs).
Then, you set up a route (on A, to B) - `ip route add 10.2.0.0/16 via 172.17.0.2 dev wg0`.

This will fail, because `wg0` will not be aware of a peer that is known to have `10.2.0.0/16` behind it.

Instead, you should make the allowed IPs `172.17.0.1/32,10.1.0.0/16` and `172.17.0.2/32,10.2.0.0/16` respectively.
As a nice side-effect though, your route ends up looking much simpler: `ip route add 10.0.0.0/8 dev wg0` (your own LAN has a more specific route, and wireguard can decide where to send what for everything else) - though this does assume your datacenters (or home networks / peers / etc) follow a specific pattern.

## Configuration

So let's go (roughly) over what configuration will look like in a 3 DC setup.

Our Datacenters will follow a very strict pattern: (X: DC identifier)
- local subnet: `10.X.0.0/16`
- VM/Docker subnet (for any routable targets): `172.16.X.0/24`
- WireGuard address: `172.17.0.X/24`
- WireGuard always listens on 1194 (the OpenVPN port, because we can)
- The public IP is `X.1.1.1` (again, because we can)

The 3 DCs in our example will have identifiers 1, 2 and 3.
We will pretend that `$P1` is the public key of DC 1, `$S2` (for secret) is the private key of DC 2, and so on.

We will end up with something like so:

### DC 1

```ini
[Interface]
ListenPort = 1194
PrivateKey = $S1

[Peer]
PublicKey = $P2
AllowedIPs = 10.2.0.0/16, 172.16.2.0/24, 172.17.0.2/32
Endpoint = 2.1.1.1:1194

[Peer]
PublicKey = $P3
AllowedIPs = 10.3.0.0/16, 172.16.3.0/24, 172.17.0.3/32
Endpoint = 3.1.1.1:1194
```

With a route of `ip route add 10.0.0.0/8 dev wg0` + `ip route add 172.16.0.0/16 dev wg0`.

### DC 2

```ini
[Interface]
ListenPort = 1194
PrivateKey = $S2

[Peer]
PublicKey = $P1
AllowedIPs = 10.1.0.0/16, 172.16.1.0/24, 172.17.0.1/32
Endpoint = 2.1.1.1:1194

[Peer]
PublicKey = $P3
AllowedIPs = 10.3.0.0/16, 172.16.3.0/24, 172.17.0.3/32
Endpoint = 3.1.1.1:1194
```

With a route of `ip route add 10.0.0.0/8 dev wg0` + `ip route add 172.16.0.0/16 dev wg0`.

### DC 3

```ini
[Interface]
ListenPort = 1194
PrivateKey = $S3

[Peer]
PublicKey = $P1
AllowedIPs = 10.1.0.0/16, 172.16.1.0/24, 172.17.0.1/32
Endpoint = 1.1.1.1:1194

[Peer]
PublicKey = $P2
AllowedIPs = 10.2.0.0/16, 172.16.2.0/24, 172.17.0.2/32
Endpoint = 2.1.1.1:1194
```

With a route of `ip route add 10.0.0.0/8 dev wg0` + `ip route add 172.16.0.0/16 dev wg0`.

## Automation

You may notice that this is actually quite similar for every peer.
Can we maybe automate it?

Let's figure out what information we will ultimately need:
1. a list of peers (name coded for comments (readability!))
2. a key pair per peer (regenerated every run, or cached in between)
3. the networks behind each peer
4. the endpoint behind each peer
5. the global (and/or per-peer) port
6. the valid route prefixes

I'm legitimately considering making such a "generator" tool.

If I do, I will update this post with a link to it.
