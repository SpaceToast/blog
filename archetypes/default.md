{{- $title := replace .TranslationBaseName "-" " " | title -}}
{{- $eqs := "=" | strings.Repeat ( $title | len ) -}}
+++
title = "{{ $title }}"
date = {{ .Date }}
draft = true

tags = [ ]
+++

{{ $title }}
{{ $eqs }}
